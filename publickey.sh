#!/usr/bin/env bash

github_username=$(printf "$1" | sed 's/^\///g')
curl -s "https://api.github.com/users/${github_username}/keys" | jq -r '.[].key'
